function fac(n) {
    if (n <= 1) return 1;
    return n * fac(n - 1);
}

console.time('total');
let N = 1;
const timer = setInterval(() => {
    console.log('fac(%d) = %d', N, fac(N));
    ++N;
}, 1000);

setTimeout(() => {
    clearInterval(timer);
    console.timeEnd('total');
}, 5010);


