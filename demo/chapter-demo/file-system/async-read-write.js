const {open, close, read, write} = require('fs');
const filename = './silly-text-2.txt';
const payload  = `1st line
just a silly text
and it goes on and on
on, last line here
`;
open(filename, 'w', (err, fd) => {
    if (err) throw new Error('cannot open file');
    write(fd, payload, (err, cnt) => {
        close(fd, () => {
            console.log(`written %d bytes, to %s`, cnt, filename);
            const buf = Buffer.alloc(cnt);
            open(filename, 'r', (err, fd) => {
                read(fd, buf, 0, cnt, 0, (err, cnt) => {
                    close(fd, () => {
                        console.log(`read %d bytes, from %s`, cnt, filename);
                        const same = (payload === buf.toString());
                        console.log(`restored: %s`, same);
                    });
                });
            });
        });
    });
});


