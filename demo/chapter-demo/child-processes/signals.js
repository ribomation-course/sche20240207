
process.on('SIGUSR2', () => {
    console.log('*** RECV USR2 signal ***');
});

process.on('SIGHUP', () => {
    console.log('*** RECV HUP signal ***');
});

setInterval(() => {
    console.log('CPU', process.cpuUsage());
    console.log('MEM', process.memoryUsage().heapUsed);
}, 2000);

setTimeout(() => {
    console.log('sending HUP signal to myself. PID=%d', process.pid);
    process.kill(process.pid, 'SIGHUP');
}, 5000);

console.log('started: PID=%d', process.pid);

