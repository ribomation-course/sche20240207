function fibonacci(n) {
    let f0 = 0n;
    let f1 = 1n;
    while (--n > 0) {
        [f0, f1] = [f1, f0 + f1];
    }
    return f1;
}

process.on('message', (arg) => {
    console.log('[child] arg: %s', arg);
    if (arg === 0) {
        process.exit(0);
    }

    const result = fibonacci(arg);
    console.log('[child] result: %s', result);

    process.send({
        argument: arg,
        result: result.toString()
    });
});

