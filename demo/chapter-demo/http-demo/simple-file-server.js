const {createServer} = require('http');
const {readFile}     = require('fs');
const {extname}      = require('path');
const {parse}        = require('url');

const port      = 8000;
const assetsDir = __dirname + '/assets';
const types     = {
    '.txt': 'text/plain',
    '.html': 'text/html',
    '.png': 'image/png'
};

createServer((req, res) => {
    const url  = parse(req.url);
    const file = url.pathname;
    console.debug('REQ: %s %s', req.method, file);
    readFile(assetsDir + file, (err, data) => {
        if (err) {
            res.writeHead(404, {
                'Content-Type': 'text/plain',
                'Date': new Date().toLocaleTimeString()
            });
            res.write(`sorry, cannot find ${file}`);
            console.debug('  -> 404 Not Found');
        } else {
            res.writeHead(200, {
                'Content-Type': types[extname(file)],
                'Content-Length': data.length,
                'Date': new Date().toLocaleTimeString()
            });
            res.write(data);
            console.debug('  -> 200 (%d bytes)', data.length);
        }
        res.end();
    });
}).listen(port, _ => {
    console.info('server started: http://localhost:%d/', port);
});

