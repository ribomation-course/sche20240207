const fs = require('fs');
const FS = require('fs').constants;

const filename = 'silly.txt';
let fd = 1;
fs.open(filename, FS.O_WRONLY | FS.O_CREAT | FS.O_TRUNC, (err, result) => {
    console.log('file open');
    fd = result;
});

let cnt = 0;
fs.write(fd, 'This is a silly message\n', (err, result) => {
    console.log('file written');
    cnt = result;
});

fs.close(fd, (err, result) => {
    console.log('file closed');
});

console.log('written %d bytes', cnt);


