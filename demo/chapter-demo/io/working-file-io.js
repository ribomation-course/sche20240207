const fs  = require('fs');
const FS  = require('fs').constants;
const txt = 'This is a silly message, from Node.js\n';

const filename = 'silly.txt';
fs.open(filename, FS.O_WRONLY | FS.O_CREAT | FS.O_TRUNC, (err, fd) => {
    if (err) {console.error(err); process.exit(1);}
    console.log('file %s opened', filename);

    fs.write(fd, txt, (err, cnt) => {
        if (err) {console.error(err); process.exit(1);}
        console.log('file %s written', filename);

        fs.close(fd, (err, ok) => {
            if (err) {console.error(err); process.exit(1);}
            console.log('file %s closed', filename);
            console.log('written %d bytes to %s', 
                        cnt, filename);
        });
    });
});

