const { Transform } = require('stream');

class ReplaceStream extends Transform {
    constructor() { super(); }
    _transform(input, enc, next) {
        const buffer = [];
        for (const val of input.values()) {
            if (lower(val)) buffer.push(toupper(val));
            else if (upper(val)) buffer.push(tolower(val));
            else if (digit(val)) buffer.push(hash);
            else buffer.push(dot);
        }
        const output = buffer
            .map(code => String.fromCharCode(code))
            .join('');
        this.push(output);
        next();
    }
}

const replace = new ReplaceStream();
replace.on('finish', () => { console.log('//DONE') });
process.stdin
    .pipe(replace)
    .pipe(process.stdout)
    ;


const hash = '#'.charCodeAt(0);
const dot = '.'.charCodeAt(0);
const a = 'a'.charCodeAt(0);
const z = 'z'.charCodeAt(0);
const A = 'A'.charCodeAt(0);
const Z = 'Z'.charCodeAt(0);
const zero = '0'.charCodeAt(0);
const nine = '9'.charCodeAt(0);
const tolower = ch => a + (ch - A);
const toupper = ch => A + (ch - a);
const between = (lb, x, ub) => (lb <= x) && (x <= ub);
const lower = ch => between(a, ch, z);
const upper = ch => between(A, ch, Z);
const digit = ch => between(zero, ch, nine);
