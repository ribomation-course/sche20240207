const { Transform } = require('stream');

class JsonArrayTransformer extends Transform {
    constructor(pretty = false) {
        super({
            readableObjectMode: true,
            writableObjectMode: true
        });
        this.leftBracketEmitted = false;
        this.firstObjectEmitted = false;
        this.prettyPrint        = pretty;
    }
    _transform(object, enc, next) {
        if (!this.leftBracketEmitted) {
            this.push('[\n');
            this.leftBracketEmitted = true;
        }
        if (this.firstObjectEmitted) {
            this.push(',\n');
        } else {
            this.firstObjectEmitted = true;
        }
        this.push(JSON.stringify(object, null, this.prettyPrint ? 2: 0));
        next();
    }
    _final(next) {
        this.push('\n]\n');
        next()
    }
}

module.exports = JsonArrayTransformer;

