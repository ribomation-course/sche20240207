const { Transform } = require('stream');

class Array2ObjectTransformer extends Transform {
    constructor() {
        super({
            readableObjectMode: true,
            writableObjectMode: true
        });
        this.header = undefined
    }
    _transform(array, enc, next) {
        if (this.header) {
            const obj = {};
            const N = Math.min(this.header.length, array.length);
            for (let k = 0; k < N; ++k) {
                const key = this.header[k];
                const val = array[k];
                obj[key]  = val;
            }
            this.push(obj);
        } else {
            this.header = array;
        }
        next();
    }

}

module.exports = Array2ObjectTransformer;

