const { Writable } = require('stream');

class ObjectWritable extends Writable {
    constructor() {
        super({
            objectMode:true
        });
    }
    _write(data, enc, next) {
        console.log(JSON.stringify(data));
        next();
    }
}

module.exports = ObjectWritable;
