const { spawn } = require('child_process');

const dir = process.argv[2] || __dirname;
const ls  = spawn('ls', ['-lhFA', dir]);
ls.stdout.on('data', (data) => {
    console.error('---data---\n', data.toString(), '---end---');
});
ls.stderr.on('data', (data) => {
    console.error('---error---\n', data.toString(), '---end---');
});
ls.on('close', (code) => {
    console.warn(`child closed stdio with code ${code}`);
});
ls.on('exit', (code) => {
    console.debug(`child terminated with exit code ${code}`);
});
ls.on('error', (err) => {
    console.error('**', err.message);
});


