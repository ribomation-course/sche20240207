create table products (
	id INT,
	name VARCHAR(50),
	price DECIMAL(5,2),
	vendor VARCHAR(50),
	country VARCHAR(50)
);
insert into products (id, name, price, vendor, country) values (1, 'Plastic Wrap', 56.7, 'Harber-Hoeger', 'Thailand');
insert into products (id, name, price, vendor, country) values (2, 'Tomato - Tricolor Cherry', 56.27, 'Feest-McCullough', 'Sweden');
insert into products (id, name, price, vendor, country) values (3, 'Onions - Dried, Chopped', 97.59, 'Donnelly Group', 'Poland');
insert into products (id, name, price, vendor, country) values (4, 'Bread - Crusty Italian Poly', 50.51, 'McKenzie Inc', 'Sweden');
insert into products (id, name, price, vendor, country) values (5, 'Pepper - Yellow Bell', 57.94, 'Dicki-Keebler', 'Sweden');
insert into products (id, name, price, vendor, country) values (6, 'Pasta - Cappellini, Dry', 39.89, 'Steuber-Abernathy', 'Norway');
insert into products (id, name, price, vendor, country) values (7, 'Bar Bran Honey Nut', 85.45, 'Spencer Group', 'Indonesia');
insert into products (id, name, price, vendor, country) values (8, 'Crush - Orange, 355ml', 7.05, 'Marquardt, Mraz and Windler', 'Denmark');
insert into products (id, name, price, vendor, country) values (9, 'Gatorade - Lemon Lime', 14.73, 'Robel, Quigley and Effertz', 'Finland');
insert into products (id, name, price, vendor, country) values (10, 'Lobster - Live', 96.7, 'Feest, Funk and Little', 'Czech Republic');
insert into products (id, name, price, vendor, country) values (11, 'Cup - Translucent 7 Oz Clear', 77.13, 'Wintheiser, Leuschke and Doyle', 'Mexico');
insert into products (id, name, price, vendor, country) values (12, 'Soup - Campbells Chicken', 57.76, 'Labadie-Tromp', 'Brazil');
insert into products (id, name, price, vendor, country) values (13, 'Cod - Black Whole Fillet', 64.26, 'Erdman and Sons', 'United Kingdom');
insert into products (id, name, price, vendor, country) values (14, 'Chocolate - Milk Coating', 53.89, 'VonRueden-Abernathy', 'Argentina');
insert into products (id, name, price, vendor, country) values (15, 'Beef - Prime Rib Aaa', 68.82, 'Monahan LLC', 'Venezuela');
insert into products (id, name, price, vendor, country) values (16, 'Figs', 4.52, 'Mann, Towne and Schmeler', 'Mongolia');
insert into products (id, name, price, vendor, country) values (17, 'Cheese - Cream Cheese', 67.59, 'Zboncak Inc', 'Indonesia');
insert into products (id, name, price, vendor, country) values (18, 'Devonshire Cream', 63.56, 'Gorczany LLC', 'Germany');
insert into products (id, name, price, vendor, country) values (19, 'Muffin - Blueberry Individual', 51.42, 'Farrell, Funk and Harris', 'Iceland');
insert into products (id, name, price, vendor, country) values (20, 'Knife Plastic - White', 99.57, 'Stark, Hauck and Wisozk', 'Sweden');
insert into products (id, name, price, vendor, country) values (21, 'Mushroom - Chanterelle Frozen', 83.25, 'Feil-Lakin', 'Brazil');
insert into products (id, name, price, vendor, country) values (22, 'Munchies Honey Sweet Trail Mix', 34.52, 'Anderson, Heathcote and Kessler', 'Thailand');
insert into products (id, name, price, vendor, country) values (23, 'Wine - Chianti Classico Riserva', 34.51, 'Huels, Baumbach and Reichert', 'Sweden');
insert into products (id, name, price, vendor, country) values (24, 'Cranberries - Dry', 33.23, 'Kuhic-Stiedemann', 'France');
insert into products (id, name, price, vendor, country) values (25, 'The Pop Shoppe - Root Beer', 82.26, 'Bogisich-Botsford', 'Poland');
