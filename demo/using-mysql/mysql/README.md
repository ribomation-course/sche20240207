# Brief Instructions

The instructions assume usage of Docker on a Linux system, such as Ubuntu @ WSL2 @ Windows 11.


## Prepare
Change first to the `./mysql` folder.

    cd ./mysql

Review the settings in `common-vars.sh`. Probably, you must update the
IP number to the server machine (`HOST`).


## Create Docker Container
Invoke this command to create a container with a MySQL 8 database.

    ./create.sh

## Start the Container
Invoke this command to start the container and therefore the database server.

    ./start.sh

## Populate with Data
Invoke this command to pre-populate the databse with some product data.

    ./mysql-cli.sh < ./products.sql

## Open a Client to the DB
Use a SQL client such as DataGrip or the mysql command-line to connect to the DB.

    ./mysql-cli.sh

## Stop the Container
Invoke this command to stop the container.

    ./stop.sh

## Remove the Container
Invoke this command to remove the container and its resources.

    ./remove.sh

