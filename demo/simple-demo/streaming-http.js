const http = require('http');
const port = 5000;
http
    .createServer((req, res) => {
        console.info('client connected');
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.write('Hi ');
        res.write('There\r\n');

        setTimeout(() => {
            res.write('Cherio\r\n');
            res.end();
        }, 1000);
    })
    .listen(port);
console.info('listing on port ', port);
