import { sum } from '../src/sum.js';

test('simple', () => {
    expect(sum(10)).toBe(55);
});

test('negative', () => {
    expect(sum(-1)).toBe(0);
});

test('zero', () => {
    expect(sum(0)).toBe(0);
});

test('one', () => {
    expect(sum(1)).toBe(1);
});

test('large', () => {
    expect(sum(1000)).toBe(500500);
});
