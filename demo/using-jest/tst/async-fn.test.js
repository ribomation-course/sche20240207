import { magic, notMagic } from "../src/async-fn.js";

test('resolving promise', async () => {
    const result = await magic(42, 250);
    expect(result).toBe(42);
});

test('resolving promise 2', async () => {
    const result = await magic('tjabba habba', 250);
    expect(result).toBe('tjabba habba');
});

test('rejecting promise', async () => {
    try {
        await notMagic(42, 250);
    } catch (err) {
        expect(err).toBe(42);
    }
});

