import { bailOut } from "../src/exception.js";

test('should throw an exception', () => {
    expect(() => {
        bailOut('Geronimoooo')
    }).toThrow(/nimo/)
});


