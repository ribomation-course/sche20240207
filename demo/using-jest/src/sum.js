export function sum(n) {
    if (n <= 0)  return 0;
    return n * (n + 1) / 2;
}

// export function dummy(x) {
//     return 42 * x;
// }


