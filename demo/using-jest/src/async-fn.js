export function magic(value, delay) {
    return new Promise((pass, fail) => {
        setTimeout(() => {
            pass(value);
        }, delay)
    });
}

export function notMagic(value, delay) {
    return new Promise((pass, fail) => {
        setTimeout(() => {
            fail(value);
        }, delay)
    });
}

