export function mkDigit() {
    const zero = '0'.charCodeAt();
    const nine = '9'.charCodeAt();
    const value = zero + Math.random() * (nine - zero);
    return String.fromCharCode(Math.floor(value));
}

export function mkNumber(pattern) {
    return pattern.replaceAll(/#/g, mkDigit);
}


