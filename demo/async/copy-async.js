const FS        = require('fs');
const PATH      = require('path');
const PROMISIFY = require('util').promisify;

const open  = PROMISIFY(FS.open);
const fstat = PROMISIFY(FS.fstat);
const read  = PROMISIFY(FS.read);
const write = PROMISIFY(FS.write);
const close = PROMISIFY(FS.close);

async function copyFile(fromFile, toFile) {
    try {
        let fd   = await open(fromFile, 'r');
        let stat = await fstat(fd);
        let buf  = Buffer.alloc(stat.size);
        let data = await read(fd, buf, 0, buf.length, null);
        let text = data.buffer.toString();
        await close(fd);
        console.log('read', data.bytesRead, 'bytes from', fromFile);

        fd   = await open(toFile, 'w');
        data = await write(fd, text.toUpperCase());
        await close(fd);
        console.log('written', data.bytesWritten, 'bytes to', toFile);
    } catch(err) {
        console.error(err);
    }
}

const filename = process.argv[2] || PATH.basename(__filename);
copyFile(filename, 'xxx-' + PATH.basename(filename, '.js') + '.txt');

