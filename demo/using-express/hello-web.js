import express from "express";
const port = 3000;
const app = express();

const simpleLogger = (req, res, next) => {
    console.log('%s %s', req.method, req.originalUrl);
    next();
};
app.use(simpleLogger);

app.get('/', (req, res) => {
    res.send({
        message: 'Hi there, from an Express.js app!',
        date: new Date().toLocaleTimeString('sv')
    });
});

app.use((req, res, next) => {
    res.status(404).send({
        message: 'Not found',
        path: req.url
    })
});

app.listen(port, () => {
    console.log('server running: http://localhost:%d/', port);
});

