// rendering of ad-hoc templates (*.hoc)
// Key syntax: @key@
import { readFile } from "node:fs/promises";
export const EXT = 'hoc';

function substitute(tmplText, model) {
    let html = tmplText;
    Object.entries(model).forEach(([key, val]) => {
        console.log(' [adhoc] %s -> %o', key, val);
        html = html.replaceAll(new RegExp(`@${key}@`, 'gi'), val);
    });
    return html;
};

export async function renderer(filePath, opts, next) {
    console.log(' [adhoc] file: %s', filePath);
    let model = Object.assign({}, opts);
    delete model.settings;
    delete model.cache;
    delete model._locals;
    try {
        const tmpl = (await readFile(filePath, 'utf8')).toString();
        const html = substitute(tmpl, model);
        return next(null, html);
    } catch (err) {
        return next(err);
    }
};

