import express from "express";
import {
    EXT as adhocExt,
    renderer as adhocRenderer
} from "./adhoc-template-engine.js";

const port = 3000;
const app = express();

app.engine(adhocExt, adhocRenderer);
app.set('view engine', adhocExt);
app.set('views', './views');

app.use(express.static('assets', {
    maxAge: '5s',
    setHeaders: (res, path, stat) => {
        res.set('x-server-name', 'The TjollaHopp Web Server, version 0.42');
    }
}));

app.use((req, _, next) => {
    console.log('%s %s', req.method, req.originalUrl);
    next();
});

app.get('/', (req, res) => {
    res.render('index', {
        time: new Date().toLocaleTimeString('sv'),
        message: 'Express.js is a cool Node.js framework!'
    });
});

app.use((req, res, next) => {
    res.status(404).send({ message: 'Not found', path: req.url })
});

app.listen(port, () => {
    console.log('server running: http://localhost:%d/', port);
});


