import fetch from "node-fetch";
const url = '  http://localhost:3000/users';

async function all() {
    const res = await fetch(url);
    if (res.ok) {
        const data = res.json();
        return data;
    } else {
        throw new Error(`HTTP ${res.status} ${res.statusText}`);
    }
}

async function one(id) {
    const res = await fetch(`${url}/${id}`);
    if (res.ok) {
        const data = res.json();
        return data;
    } else {
        throw new Error(`HTTP ${res.status} ${res.statusText}`);
    }
}

async function create(data) {
    const opts = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
    };
    const res = await fetch(url, opts);
    if (res.ok) {
        const data = res.json();
        return data;
    } else {
        throw new Error(`HTTP ${res.status} ${res.statusText}`);
    }
}

async function update(id, data) {
    const opts = {
        method: 'PATCH',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' }
    };
    const res = await fetch(`${url}/${id}`, opts);
    if (res.ok) {
        const data = res.json();
        return data;
    } else {
        throw new Error(`HTTP ${res.status} ${res.statusText}`);
    }
}

async function remove(id) {
    const opts = { method: 'DELETE' };
    const res = await fetch(`${url}/${id}`, opts);
    if (res.ok) {
        return true;
    } else {
        throw new Error(`HTTP ${res.status} ${res.statusText}`);
    }
}


// console.log('all: ', await all());

const nisse = {
    first_name: 'Nisse', 
    last_name: 'Hult', 
    email: 'nisse@gmail.com', 
    gender: 'Male'
};
const data = await create(nisse);
const id = data.id;
console.log('[Create] id:', id);

console.log('[Read]', await one(id));

const data2 = await update(id, { first_name: 'Bosse' });
console.log('[Update]', data2);

await remove(id);

try {
    await one(id);
    throw new Error('failed to delete id=' + id);
} catch (x) {
    console.log('[Delete] id:', id);
}


