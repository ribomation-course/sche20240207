const express  = require('express');
const app      = express();
const port     = 3000;

const resource = '/persons';
const names    = [
    'Anna Conda', 'Anna Gram', 'Justin Time',
    'Per Silja', 'Inge Vidare', 'Åke R. Vidare',
    'Sham Poo', 'A. U. Thor'
];
const age      = () => Math.round(20 + 60 * Math.random());
let nextId     = 1;
const db       = names.map(name => {
    return {id: nextId++, name: name, age: age()};
});


app.use((req, res, next) => {
    console.info('REQ: %s %s', req.method, req.originalUrl);
    next();
});
app.use(express.json());
app.use(express.static(__dirname + '/assets'));


app.route(resource)
    .get((req, res) => {
        res.json(db);
    })
    .post((req, res) => {
        const obj = req.body;
        obj.id    = 1 + Math.max(...db.map(o => o.id));
        db.push(obj);
        res.status(201).json(obj);
    });

app.route(resource + '/:id')
    .all((req, res, next) => {
        const id  = Number(req.params.id);
        const idx = db.findIndex(o => o.id === id);
        if (idx === -1) {
            res.sendStatus(404);
        } else {
            req.resourceIndex = idx;
            req.resource      = db[idx];
            next();
        }
    })
    .get((req, res) => {
        res.json(req.resource);
    })
    .put((req, res) => {
        const obj  = req.resource;
        const obj2 = req.body;
        ['name', 'age'].forEach(key => {
            if (obj2[key]) obj[key] = obj2[key];
        });
        res.json(obj);
    })
    .delete((req, res) => {
        db.splice(req.resourceIndex, 1);
        res.sendStatus(204);
    });

app.listen(port, () => {
    console.info('started: http://localhost:%d/persons', port);
});
