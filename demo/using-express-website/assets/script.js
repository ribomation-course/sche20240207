const loadTime     = 5000;
const numUpdates   = 5;
const itvPeriod    = loadTime / numUpdates;
const percentDelta = 100 / numUpdates;
let percent        = 0;

const itvId = setInterval(() => {
    const payload = document.querySelector('#payload');
    payload.textContent = `Processing server...${percent.toFixed(1)}%`;
    percent += percentDelta;
}, itvPeriod);

setTimeout(async () => {
    const res = await fetch('http://localhost:3000/api');
    if (res.ok) {
        clearInterval(itvId);
        const data    = await res.json();
        const payload = document.querySelector('#payload');
        payload.textContent = JSON.stringify(data, null, 3);
    } else {
        throw new Error(`HTTP failed: ${res.status} ${res.statusText}`);
    }
}, loadTime);

