import express from "express";
const port = 3000;
const app = express();

app.use(express.static('assets', {
    maxAge: '5s', 
    setHeaders: (res, path, stat) => {
        res.set('x-server-name', 'The TjollaHopp Web Server, version 0.42');
    }
}));

app.use((req, _, next) => {
    console.log('%s %s', req.method, req.originalUrl);
    next();
});

app.get('/api', (req, res) => {
    res.send({
        message: 'Some data obtained from the server',
        date: new Date().toLocaleTimeString('sv')
    });
});

app.use((req, res, next) => {
    res.status(404).send({ message: 'Not found', path: req.url })
});

app.listen(port, () => {
    console.log('server running: http://localhost:%d/', port);
});

