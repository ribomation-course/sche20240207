import { readFile } from "node:fs/promises";
import express from "express";
const port = 3000;
const app = express();

let db = [];

async function loadDb() {
    const data = (await readFile('./db.json', 'utf8')).toString();
    const db = JSON.parse(data);
    console.log('loaded %s records', db.length);
    return db;
}

app.use(express.json());

app.use((req, _, next) => {
    console.log('%s %s', req.method, req.originalUrl);
    next();
});

app.get('/products', (req, res) => {
    res.send(db);
});

app.get('/products/:id', (req, res) => {
    const id = Number(req.params.id || '-1');
    const obj = db.find(o => o.id === id);
    if (obj) {
        res.send(obj);
    } else {
        res.sendStatus(404);
    }
});

app.post('/products', (req, res) => {
    const obj = req.body;
    obj.id    = 1 + Math.max(...db.map(o => o.id));
    obj.price = Number(obj.price);
    db.push(obj);
    res.status(201).send(obj);
});

app.patch('/products/:id', (req, res) => {
    const id = Number(req.params.id || '-1');
    const idx = db.findIndex(o => o.id === id);
    if (idx > -1) {
        const delta = req.body;
        const updatedObj = Object.assign(db[idx], delta);
        db.splice(idx, 1, updatedObj);
        res.send(db[idx]);
    } else {
        res.sendStatus(404);
    }
});

app.delete('/products/:id', (req, res) => {
    const id = Number(req.params.id || '-1');
    const idx = db.findIndex(o => o.id === id);
    if (idx > -1) {
        db.splice(idx, 1);
        res.sendStatus(204);
    } else {
        res.sendStatus(404);
    }
});

app.use((req, res, next) => {
    res.status(404).send({ message: 'Not found', path: req.url })
});

app.listen(port, async () => {
    db = await loadDb();
    console.log('server running: http://localhost:%d/', port);
});


