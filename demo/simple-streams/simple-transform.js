const { Transform } = require('stream');

const uppercase = new Transform({
    transform(buf, enc, next) {
        const payload = buf.toString();
        this.push(payload.toUpperCase());
        next();
    }
});

process.stdin
    .pipe(uppercase)
    .pipe(process.stdout);

