const {Writable} = require('stream');

const outStream = new Writable({
    write(buf, enc, next) {
        console.info('*', buf.length/1024, 'KB', enc);
        next();
    }
});

process.stdin.pipe(outStream);
