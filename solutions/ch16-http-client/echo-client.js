const {request} = require('http');


let payload = process.argv[2] || 'Tjabba Habba Babba';

const cfg = {
    host: 'localhost',
    port: 8000,
    method: 'POST',
    path: '/',
    headers: {
        'Content-Type': 'text/plain',
        'Content-Length': payload.length
    }
};

const resHdlr = (res) => {
    console.info('RES: %d %s', res.statusCode, res.statusMessage);
    res.pipe(process.stdout);
};

const req = request(cfg, resHdlr);
req.write(payload);
req.end();
req.on('error', (err) => {
    console.error('failed:', err);
});
