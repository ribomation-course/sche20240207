export function reverseWords(sentence) {
    if (sentence === null || sentence === undefined) {
        throw new Error('empty sentence');
    }
    if (/^\s*$/.test(sentence)) {
        return '';
    }
    return sentence.split(/\s+/).reverse().join(' ');
}
