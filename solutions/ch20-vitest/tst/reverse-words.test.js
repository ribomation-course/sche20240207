import {reverseWords} from '../src/reverse-words.js'
import { expect, test } from 'vitest'


test('simple sentence', () => {
    expect(reverseWords('hej Hopp hIPP'))
        .toBe('hIPP Hopp hej');
})

test('one word', () => {
    expect(reverseWords('hejsan'))
        .toBe('hejsan');
})

test('empty string', () => {
    expect(reverseWords(''))
        .toBe('');
})

test('space string', () => {
    expect(reverseWords('        '))
        .toBe('');
})

test('null argument', () => {
    expect(() => { reverseWords(null); })
        .toThrow();
})

test('undefined argument', () => {
    let value;
    expect(() => { reverseWords(value); })
        .toThrow();
})

