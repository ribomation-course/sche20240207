import { EventEmitter } from 'node:events';

class EvenOddEmitter extends EventEmitter {
    handle(n) {
        const eventName = (n % 2 === 0) ? 'even' : 'odd';
        console.debug('[EvenOddEmitter] %s: %d', eventName, n);
        this.emit(eventName, n);
    }
}

await main();

// --- Subs ---

async function main() {
    const evenLst = [];
    const oddLst  = [];
    const emmi    = new EvenOddEmitter();

    emmi.on('even', (n) => { evenLst.push(n); });
    emmi.on('odd' , (n) => { oddLst.push(n); });
    
    const intvId = launchEmitter(200, emmi);
    await waitFor(5);
    console.log('-- done --');

    clearInterval(intvId);
    emmi.removeAllListeners();

    console.log('even: %s', sortNumeric(evenLst));
    console.log('odd : %s', sortNumeric(oddLst));
}

function launchEmitter(numMilliSecs, emmi) {
    return setInterval(() => {
        const num = randInt(0, 300);
        emmi.handle(num);
    }, numMilliSecs);
}

async function waitFor(numSecs) {
    return new Promise((done,_) => {
        setTimeout(() => { done(); }, numSecs * 1000);
    });
}

function randInt(lb = 1, ub = 1000) {
    const width = ub - lb + 1;
    const r     = Math.random() * width;
    return lb + Math.floor(r);
}

function sortNumeric(list) {
    const byNumber = (a,b) => {return a-b;};
    return list.sort(byNumber);
}
