# Launch server

    $ npm run start

# Invoke echo
Usage of HTTPie https://httpie.io/

    $ http -v :8000 name=nisse age=42 city=stockholm
    POST / HTTP/1.1
    Accept: application/json, */*;q=0.5
    Accept-Encoding: gzip, deflate
    Connection: keep-alive
    Content-Length: 51
    Content-Type: application/json
    Host: localhost:8000
    User-Agent: HTTPie/3.2.2
    
    {
    "age": "42",
    "city": "stockholm",
    "name": "nisse"
    }
    
    
    HTTP/1.1 200 OK
    Connection: keep-alive
    Date: Sat, 10 Feb 2024 21:49:52 GMT
    Keep-Alive: timeout=5
    Transfer-Encoding: chunked
    
    {"NAME": "NISSE", "AGE": "42", "CITY": "STOCKHOLM"}


