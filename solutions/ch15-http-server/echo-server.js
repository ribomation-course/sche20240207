const { createServer } = require('http');
const transform = require('through2');
const port = 8000;

const srv = createServer((req, res) => {
    console.debug('REQ: %s %s', req.method, req.url);
    const upper = function(data, _env, next)  {
        this.push(data.toString().toUpperCase());
        next();
    };
    req.pipe(transform(upper)).pipe(res);
});

srv.listen(port, () => {
    console.info('server started: http://localhost:%d/', port)
})
