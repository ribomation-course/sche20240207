import resource from './person-dao.js';

const personSchema = {
    type: 'object',
    properties: {
        id: {type: 'number'},
        name: {type: 'string'},
        age: {type: 'number'},
    },
    required: ['name', 'age']
}

async function routes(srv, options) {
    resource.initDB();

    const getAllSchema = {
        response: {
            200: {
                type: 'array',
                items: personSchema
            }
        }
    }
    srv.get('/', {schema: getAllSchema}, async (req, reply) => {
        return resource.all();
    });

    const getOneSchema = {
        response: {
            200: personSchema,
            404: {
                message: {type: 'string'}
            }
        }
    };
    srv.get('/:id', {schema: getOneSchema}, async (req, reply) => {
        const {id} = req.params;
        const {index, object} = resource.lookup(id);
        if (index >= 0) {
            return object;
        }
        reply.status(404).send({
            message: `object id=${id} was not found`
        });
    });

    const createSchema = {
        body: {...personSchema},
        response: {
            201: personSchema
        }
    };
    srv.post('/', {schema: createSchema}, async (req, reply) => {
        const {name, age} = req.body;
        const obj = resource.create({name, age});
        reply.code(201).send(obj);
    });

    const removeSchema = {
        response: {
            204: {},
            404: {
                message: {type: 'string'}
            }
        }
    };
    srv.delete('/:id', {schema: removeSchema}, async (req, reply) => {
        const {id} = req.params;
        const {index, object} = resource.lookup(id);
        if (index >= 0) {
            resource.remove(index);
            reply.code(204).send(
                {
                    message: `object id=${id} was removed`
                }
            );
        } else {
            reply.status(404).send({
                message: `object id=${id} was not found`
            });
        }
    });

    const updateSchema = {
        body: {...personSchema, required:[]},
        response: {
            200: personSchema
        }
    };
    srv.put('/:id', {schema: updateSchema}, async (req, reply) => {
        const {id} = req.params;
        const {index, object} = resource.lookup(id);
        if (index >= 0) {
            return resource.update(object, req.body);
        }else {
            reply.status(404).send({
                message: `object id=${id} was not found`
            });
        }
    });

}

export default routes;
