import {fileURLToPath} from 'node:url';
import {dirname, join} from 'node:path';
import process from "node:process";

import Fastify from 'fastify';
import assetsHandler from '@fastify/static';
import swaggerEngine from '@fastify/swagger';
import swaggerUI from '@fastify/swagger-ui';


import routes from './routes.js';

const port = 4000;
const dir = dirname(fileURLToPath(import.meta.url));

const app = Fastify({
    logger: true
});

app.register(assetsHandler, {
    root: join(dir, './assets'),
    prefix: '/'
});

app.register(swaggerEngine, {
    swagger: {
        info: {
            title: 'Silly Persons - API Docs',
            description: 'Fastify Swagger/Open-API generated docs',
            version: '0.1.0'
        },
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
    }
});
app.register(swaggerUI, {
    routePrefix: '/docs',
    uiConfig: {
        docExpansion: 'list',
        deepLinking: false
    },
});

app.register(routes, {
    prefix: '/person'
});


const launch = async () => {
    try {
        const address = await app.listen({port, host: '0.0.0.0'});
        console.info('started:', address);
    } catch (err) {
        console.error('failed: %o', err);
        process.exit(1);
    }
};
await launch();
