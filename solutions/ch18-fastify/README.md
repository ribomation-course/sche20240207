# Beskrivning
Återimplementation av uppgiften _Kap18 Express.js_, men där express
är utbytt mot Fastify.

Förutom implementation av samma funktionalitet som för express,
så exponerar servern genererad documentation med Open-API/Swagger.

# Uppstart
Börja med att installera alla beroenden

    npm i

# Start av servern
För att starta dev servern, med automatisk omstart när man sparar
en fil, skriv

    npm run dev

För att starta servern i production mode, skriv

    npm run start

Server ligger nu och lyssnar på port `4000` och URL
http://localhost:4000/


# REST webbtjänst
REST operationerna ligger på prefix `/person`

Med en HTTP klient som [HTTPie](https://httpie.io/cli), så kan man
anropa samtliga end-points

    http :4000/person
    http :4000/person/1
    http :4000/person name=Nisse age=42
    http put :4000/person name=Anna
    http delete :4000/person/1

# Swagger UI
Den genererade och interaktiva dokumentationen finns på URL
http://localhost:4000/docs

