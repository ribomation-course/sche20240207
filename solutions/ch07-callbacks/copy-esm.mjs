import * as fs from "fs";

const name = "test.txt";

fs.readFile(name, "utf8", (err, data) => {
    if (err) {
        console.error(err);
        console.log("Cant read the file");
        return;
    }
    console.log(data, "data");
    fs.writeFile(name + ".copy", data.toUpperCase(), "utf8", (err) => {
        if (err) {
            console.error(err);
            console.log("Cant write the file");
        }
        console.log("written");
    });
});