class Chars {
    constructor(firstChar, lastChar) {
        const first = firstChar.charCodeAt();
        const last = lastChar.charCodeAt();
        const numChars = last - first + 1;
        this.buf = Buffer.alloc(numChars);
        for (let ch = first, k = 0; ch <= last; ++ch, ++k) {
            this.buf[k] = ch;
        }
    }
}

function print(buf) {
    for (let k = 0; k < buf.length; ++k) {
        console.log('ASCII %d = "%s"', buf[k], String.fromCharCode(buf[k]));
    }
}

print(Buffer.concat([
    new Chars('0', '9').buf, 
    new Chars('A', 'Z').buf, 
    new Chars('a', 'z').buf
]));

