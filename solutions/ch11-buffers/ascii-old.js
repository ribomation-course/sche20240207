const digitBegin     = '0'.charCodeAt()
const digitEnd       = '9'.charCodeAt()
const digitCount     = digitEnd - digitBegin + 1
const letterUpBegin  = 'A'.charCodeAt()
const letterUpEnd    = 'Z'.charCodeAt()
const letterUpCount  = letterUpEnd - letterUpBegin + 1
const letterLowBegin = 'a'.charCodeAt()
const letterLowEnd   = 'z'.charCodeAt()
const letterLowCount = letterLowEnd - letterLowBegin + 1

const digit = Buffer.alloc(digitCount);
const upper = Buffer.alloc(letterUpCount);
const lower = Buffer.alloc(letterLowCount);
const N     = lower.length + upper.length + digit.length;

for (let k = digitBegin    , idx = 0; k <= digitEnd    ; ++k, ++idx) { digit[idx] = k; }
for (let k = letterUpBegin , idx = 0; k <= letterUpEnd ; ++k, ++idx) { upper[idx] = k; }
for (let k = letterLowBegin, idx = 0; k <= letterLowEnd; ++k, ++idx) { lower[idx] = k; }

function print(buf) {
    for (let k = 0; k < buf.length; ++k) {
        console.log('%d: %s', buf[k], String.fromCharCode(buf[k]));
    }
}

print(Buffer.concat([digit, upper, lower], N));
