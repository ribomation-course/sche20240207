import {Readable} from 'node:stream';

class FibonacciStream extends Readable {
    constructor(max) {
        super();
        this.max = max;
        this.cnt = 1;
        this.f0 = 0n;
        this.f1 = 1n;
    }

    _read(sz) {
        const result = `${this.cnt}: ${this.f1}\n`;
        this.push(Buffer.from(result, 'ascii'));
        if (++this.cnt > this.max) this.push(null);
        
        [this.f1, this.f0] = [this.f0 + this.f1,  this.f1];
    }
}

const N = Number(process.argv[2] || '250');
new FibonacciStream(N).pipe(process.stdout);

