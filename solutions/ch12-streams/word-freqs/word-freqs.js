import {Transform} from 'node:stream';

class WordExtracter extends Transform {
    constructor() {
        super();
    }
    _transform(chunk, enc, next) {
        const words = chunk.toString().split(/[^a-z]+/ig);
        words.forEach(word => this.push(word.toLowerCase()));
        next();
    }
}

class FreqAggregator extends Transform {
    constructor(maxWordCount, minWordLength) {
        super();
        this.maxWordCount = maxWordCount;
        this.minWordLength = minWordLength;
        this.freqs = new Map();
    }

    _transform(word, enc, next) {
        //console.debug('word: [%s]', word);
        if (word.length >= this.minWordLength) {
            word = word.toString();
            const n = +(this.freqs.get(word)) || 0;
            this.freqs.set(word, n + 1);
        }
        next();
    }

    _final(next) {
        //console.debug('freqs: [%o]', this.freqs);
        [...this.freqs.entries()]
            .sort((lhs, rhs) => (rhs[1] - lhs[1]))
            .slice(0, this.maxWordCount)
            .map(pair => `${pair[0]}: ${pair[1]}\n`)
            .forEach(str => this.push(str))
            ;
        next();
    }
}


//usage: cat ../../files/musketeers.txt | node word-freqs.js [max-words] [min-size]

const maxWords = +(process.argv[2]) || 25;
const minSize  = +(process.argv[3]) || 5;

process.stdin
    .pipe(new WordExtracter())
    .pipe(new FreqAggregator(maxWords, minSize))
    .pipe(process.stdout)
    ;

