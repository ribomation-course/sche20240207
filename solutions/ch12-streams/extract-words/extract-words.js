import {Transform} from 'node:stream';

class WordExtracter extends Transform {
    constructor() {
        super();
    }
    _transform(chunk, enc, next) {
        const words = chunk.toString().split(/[^a-z]+/ig);
        words.forEach(word => this.push(word));
        next();
    }
}

class Newline extends Transform {
    constructor() {
        super();
    }
    _transform(word, enc, next) {
        this.push(word + '\n');
        next();
    }
}


//usage: cat extract-words.js | env node extract-words.js
process.stdin
    .pipe(new WordExtracter())
    .pipe(new Newline())
    .pipe(process.stdout);

