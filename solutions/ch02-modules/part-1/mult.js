
let value = 10;

function multiply(n) {
    return n * value;
}

function factor(x) {
    value = x;
}

module.exports = {
    multiply, 
    factor
};
