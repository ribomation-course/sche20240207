export const URI = '/persons';

const names = [
    'Anna Conda', 'Anna Gram', 'Justin Time',
    'Per Silja', 'Inge Vidare', 'Åke R. Vidare',
    'Sham Poo', 'A. U. Thor'
];
const age = () => Math.round(20 + 60 * Math.random());
const replacable_properties = ['name', 'age'];
let db = [];

function initDB() {
    let nextId = 1;
    db = names.map(name => {
        return {
            id: nextId++,
            name: name,
            age: age()
        };
    });
}

function all() {
    return db;
}

function create(obj) {
    obj.id = 1 + Math.max(...db.map(o => o.id));
    db.push(obj);
    return obj;
}

function lookup(id) {
    id = Number(id);
    const idx = db.findIndex(o => o.id === id);
    return { index: idx, object: db[idx] }
}

function update(object, replacement) {
    replacable_properties.forEach(key => {
        if (replacement[key] !== undefined) {
            object[key] = replacement[key];
        }
    });
    return object;
}

function remove(index) {
    db.splice(index, 1);
}

export default  {
    URI, initDB, all, create, lookup, update, remove
}

