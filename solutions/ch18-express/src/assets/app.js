

async function loadAll() {
    const res = await fetch('http://localhost:4000/persons');
    if (res.ok) {
        return await res.json();
    }
    return [];
}

async function populate() {
    const objs = await loadAll();
    const tbody = document.querySelector('tbody');
    tbody.innerHTML = '';
    objs.forEach(obj => {
        const tr = document.createElement('tr');
        tr.innerHTML = `
            <td> ${obj.id} </td> <td> ${obj.name} </td> <td> ${obj.age} </td>
        `;
        tbody.appendChild(tr);
    });

    const loading = document.querySelector('#loading');
    loading.style = 'display: none';
}

const loadTime = 5000;
const numUpdates = 10;
const fractionTime = loadTime / numUpdates;
let count = 0;

function progress() {
    const loading = document.querySelector('#loading');
    loading.innerHTML = `Loading ${100 * count / numUpdates}% ..., please hold`;
    ++count;
}

async function main() {
    const timerId = setInterval(progress, fractionTime);
    setTimeout(async () => {
        clearInterval(timerId);
        await populate();
    }, loadTime + fractionTime);
}

window.addEventListener('load', async () => {
    await main();
});
