CREATE DATABASE IF NOT EXISTS nodejs_db;

GRANT all ON nodejs_db.* TO 'nodejs'@'%';
flush privileges;

SELECT host, user FROM mysql.user;
show databases;

USE nodejs_db;

CREATE TABLE IF NOT EXISTS products (
	id      INT,
	name    VARCHAR(50),
	price   DECIMAL(5,2),
	vendor  VARCHAR(50),
	country VARCHAR(50)
);

INSERT INTO products (id, name, price, vendor, country) VALUES (1, 'Plastic Wrap', 56.7, 'Harber-Hoeger', 'Thailand');
INSERT INTO products (id, name, price, vendor, country) VALUES (2, 'Tomato - Tricolor Cherry', 56.27, 'Feest-McCullough', 'Sweden');
INSERT INTO products (id, name, price, vendor, country) VALUES (3, 'Onions - Dried, Chopped', 97.59, 'Donnelly Group', 'Poland');
INSERT INTO products (id, name, price, vendor, country) VALUES (4, 'Bread - Crusty Italian Poly', 50.51, 'McKenzie Inc', 'Sweden');
INSERT INTO products (id, name, price, vendor, country) VALUES (5, 'Pepper - Yellow Bell', 57.94, 'Dicki-Keebler', 'Sweden');
INSERT INTO products (id, name, price, vendor, country) VALUES (6, 'Pasta - Cappellini, Dry', 39.89, 'Steuber-Abernathy', 'Norway');
INSERT INTO products (id, name, price, vendor, country) VALUES (7, 'Bar Bran Honey Nut', 85.45, 'Spencer Group', 'Indonesia');
INSERT INTO products (id, name, price, vendor, country) VALUES (8, 'Crush - Orange, 355ml', 7.05, 'Marquardt, Mraz and Windler', 'Denmark');
INSERT INTO products (id, name, price, vendor, country) VALUES (9, 'Gatorade - Lemon Lime', 14.73, 'Robel, Quigley and Effertz', 'Finland');
INSERT INTO products (id, name, price, vendor, country) VALUES (10, 'Lobster - Live', 96.7, 'Feest, Funk and Little', 'Czech Republic');
INSERT INTO products (id, name, price, vendor, country) VALUES (11, 'Cup - Translucent 7 Oz Clear', 77.13, 'Wintheiser, Leuschke and Doyle', 'Mexico');
INSERT INTO products (id, name, price, vendor, country) VALUES (12, 'Soup - Campbells Chicken', 57.76, 'Labadie-Tromp', 'Brazil');
INSERT INTO products (id, name, price, vendor, country) VALUES (13, 'Cod - Black Whole Fillet', 64.26, 'Erdman and Sons', 'United Kingdom');
INSERT INTO products (id, name, price, vendor, country) VALUES (14, 'Chocolate - Milk Coating', 53.89, 'VonRueden-Abernathy', 'Argentina');
INSERT INTO products (id, name, price, vendor, country) VALUES (15, 'Beef - Prime Rib Aaa', 68.82, 'Monahan LLC', 'Venezuela');
INSERT INTO products (id, name, price, vendor, country) VALUES (16, 'Figs', 4.52, 'Mann, Towne and Schmeler', 'Mongolia');
INSERT INTO products (id, name, price, vendor, country) VALUES (17, 'Cheese - Cream Cheese', 67.59, 'Zboncak Inc', 'Indonesia');
INSERT INTO products (id, name, price, vendor, country) VALUES (18, 'Devonshire Cream', 63.56, 'Gorczany LLC', 'Germany');
INSERT INTO products (id, name, price, vendor, country) VALUES (19, 'Muffin - Blueberry Individual', 51.42, 'Farrell, Funk and Harris', 'Iceland');
INSERT INTO products (id, name, price, vendor, country) VALUES (20, 'Knife Plastic - White', 99.57, 'Stark, Hauck and Wisozk', 'Sweden');
INSERT INTO products (id, name, price, vendor, country) VALUES (21, 'Mushroom - Chanterelle Frozen', 83.25, 'Feil-Lakin', 'Brazil');
INSERT INTO products (id, name, price, vendor, country) VALUES (22, 'Munchies Honey Sweet Trail Mix', 34.52, 'Anderson, Heathcote and Kessler', 'Thailand');
INSERT INTO products (id, name, price, vendor, country) VALUES (23, 'Wine - Chianti Classico Riserva', 34.51, 'Huels, Baumbach and Reichert', 'Sweden');
INSERT INTO products (id, name, price, vendor, country) VALUES (24, 'Cranberries - Dry', 33.23, 'Kuhic-Stiedemann', 'France');
INSERT INTO products (id, name, price, vendor, country) VALUES (25, 'The Pop Shoppe - Root Beer', 82.26, 'Bogisich-Botsford', 'Poland');

SELECT * FROM products ORDER BY name;

