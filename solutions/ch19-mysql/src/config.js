export const web_config = {
    port: 4000
};

export const db_config = {
    host: 'localhost',
    port: '3306',
    user: 'nodejs',
    password: 'nodejs',
    database: 'nodejs_db',
};
