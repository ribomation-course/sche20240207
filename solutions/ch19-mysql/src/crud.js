import {createConnection} from "mysql2/promise";
import {db_config} from './config.js';


async function main() {
    let db;
    try {
        db = await openDB(db_config);
        await createTable(db);

        const N = 5;
        let lastId = 0;
        for (let k = 1; k <= N; ++k) {
            const name = mkName();
            const age = mkAge();
            const id = await insertRow(db, {name, age});
            lastId = id;
            console.log('[C]reate: [%d, %s, %s]', id, name, age);
        }

        const all = await readAll(db);
        all.forEach(row => console.log(row));

        const one = await readRow(db, lastId);
        console.log('[R]ead: %o', one);

        await updateRow(db, lastId, {name: one.name.toUpperCase(), age: one.age / 2});
        console.log('[U]pdate: %o', await readRow(db, lastId));

        await deleteRow(db, lastId);
        console.log('[D]elete: id=%d: %o', lastId, await readRow(db, lastId));
    } catch (err) {
        console.error('failed: %o', err)
    } finally {
        await dropTable(db);
        await shutdownDB(db);
    }
}

await main();


// --- Generator ---
function mkName() {
    const names = [
        'Anna Conda', 'Per Silja', 'Justin Time',
        'Ana Gram', 'Inge Vidare', 'Sham Poo'
    ];
    return names[Math.floor(Math.random() * names.length)];
}

function mkAge() {
    return Math.floor(18 + Math.random() * 80);
}


// --- DB ---
async function openDB(cfg) {
    return createConnection(cfg);
}

async function shutdownDB(db) {
    await db.end();
}

async function createTable(db) {
    const sql = `
        CREATE TABLE IF NOT EXISTS persons (
            id      INT PRIMARY KEY AUTO_INCREMENT,
            name    VARCHAR(32) NOT NULL,
            age     INT NOT NULL
        );
    `;
    await db.execute(sql);
}

async function dropTable(db) {
    await db.execute('DROP TABLE IF EXISTS persons');
}

async function insertRow(db, {name, age}) {
    const sql = 'INSERT INTO persons (name,age) VALUES (?,?)';
    const [result, meta] = await db.query(sql, [name, age]);
    return result.insertId;
}

async function deleteRow(db, id) {
    const sql = 'DELETE FROM persons WHERE id = ?';
    const [result, meta] = await db.query(sql, [id]);
    return result.changedRows;
}

async function updateRow(db, id, {name, age}) {
    const sql = 'UPDATE persons SET name = ?, age = ? WHERE id = ?';
    const [result, meta] = await db.query(sql, [name, age, id]);
    return result.changedRows;
}

async function readRow(db, id) {
    const sql = 'SELECT id,name,age FROM persons WHERE id = ?';
    const [result, meta] = await db.query(sql, [id]);
    if (result.length === 1) {
        return result[0];
    }
    return undefined;
}

async function readAll(db) {
    const sql = 'SELECT id,name,age FROM persons';
    const [result, meta] = await db.query(sql);
    return result;
}


