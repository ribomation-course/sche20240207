# Launch DB

    docker compose up --detach

# Test DB

    npm run crud

# Launch Server

    npm run server

# Shutdown DB

    docker compose down

