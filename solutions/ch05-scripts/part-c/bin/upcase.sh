#!/usr/bin/env node
'use strict';

const args = process.argv.slice(2);
const ARGS = args.map(word => word.toUpperCase());
console.log(ARGS.join(' '));
