# Prepare
    cd part-b

# Install
    npm install --global local-web-server

# Run
    ws --directory ./www --port 8888 --open 

# Uninstall
    npm uninstall --global local-web-server
    cd ..
