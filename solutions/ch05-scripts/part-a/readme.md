# Prepare
    cd part-a

# Install
    npm install --global cowsay

# Execute
    cowsay -f elephant2 'Node.js is really cool!'

# Uninstall
    npm uninstall -g cowsay
    cd ..
