# Beskrivning
Återimplementation av uppgiften _Kap19 Mysql & Express.js_, men där express
är utbytt mot Fastify.

Template engine är Handlebars, precis som i förlagan

# Uppstart
Säkerställ att Docker är installerad och dess server är startad.

Börja med att installera alla beroenden

    npm i

# Databas
Starta databasen

    cd mysql
    docker compose up --detach

Stoppa och radera databasen

    cd mysql
    docker compose down

# Server
För att starta dev servern, med automatisk omstart när man sparar
en fil, skriv

    npm run dev

För att starta servern i production mode, skriv

    npm run start

Server ligger nu och lyssnar på port `4000` och URL
http://localhost:4000/



