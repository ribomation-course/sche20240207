async function productsController(srv, opts) {
    if (!srv.mysql) {
        console.error('srv.mysql not defined');
    }

    srv.get('/', async (req, reply) => {
        return reply.view('/templates/index.hbs')
    });

    srv.get('/products', async (req, reply) => {
        let db;
        try {
            db = await srv.mysql.getConnection();
            const sql = 'SELECT * FROM products';
            const [rows, fields] = await db.query(sql);
            return reply.view('/templates/product-list.hbs', {products: rows})
        } catch (err) {
            console.error('failed: %o', err)
            return reply.view('/templates/product-not-found.hbs', {id: -1});
        } finally {
            if (!!db) db.release();
        }
    });

    srv.get('/product/:id', async (req, reply) => {
        const {id} = req.params;
        let db;
        try {
            db = await srv.mysql.getConnection();
            const sql = 'SELECT * FROM products WHERE id=?';
            const [rows, fields] = await db.query(sql, [id]);
            return reply.view('/templates/product-detail.hbs', {product: rows[0]});
        } catch (err) {
            console.error('failed: %o', err)
            return reply.view('/templates/product-not-found.hbs', {id: id});
        } finally {
            if (!!db) db.release();
        }
    });

}

export default productsController;


