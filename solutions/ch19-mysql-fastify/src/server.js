import {fileURLToPath} from 'node:url';
import {dirname, join} from 'node:path';
import process from "node:process";

import Fastify from 'fastify';
import assetsHandler from '@fastify/static';
import viewHandler from '@fastify/view';
import mysqlHandler from '@fastify/mysql'
import handlebarsEngine from 'handlebars'

import productsController from './controllers/products-controller.js';


const port = 4000;
const dir = dirname(fileURLToPath(import.meta.url));
console.debug('dir:', dir);


const app = Fastify({
    logger: {
        transport: {
            target: "@fastify/one-line-logger",
        },
    }
});

app.register(mysqlHandler, {
    promise: true,
    host: 'localhost',
    port: '3306',
    user: 'nodejs',
    password: 'nodejs',
    database: 'nodejs_db',
});

app.register(assetsHandler, {
    root: join(dir, './assets'),
    prefix: '/'
});

app.register(viewHandler, {
    engine: {
        handlebars: handlebarsEngine,
    },
    root: join(dir, './'),
    viewExt: 'hbs',
    layout: './templates/layouts/main.hbs',
});

app.register(productsController, {
    prefix: '/'
});

const launch = async () => {
    try {
        const address = await app.listen({port, host: '0.0.0.0'});
        console.info('started:', address);
    } catch (err) {
        console.error('failed: %o', err);
        process.exit(1);
    }
};
await launch();

