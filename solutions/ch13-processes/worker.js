process.on('message', (txt) => {
    console.log('[child] %s', txt);

    const cpu = (process.cpuUsage().system/1000).toFixed(2) + ' ms';
    const mem = (process.memoryUsage().heapUsed / (1024*1024)).toFixed(3) + ' GB';
    const v8  = process.versions.v8;
    const reply = `
    TXT: ${txt.toUpperCase()}
    CPU: ${cpu}
    MEM: ${mem}
    V8 : ${v8} `;
    process.send(reply);
});

process.on('SIGINT', () => {
    console.log('[child] done');
    process.exit(0);
});
