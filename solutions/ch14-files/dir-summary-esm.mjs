import { readdirSync, statSync } from 'node:fs';
import { join, dirname, normalize } from 'node:path';
import { fileURLToPath } from 'node:url';

class Counts {
    dirs = 0;
    files = 0;
    bytes = 0;

    updateFile(filepath) {
        const meta = statSync(filepath);
        this.files++;
        this.bytes += meta.size;
    }

    updateCounts(counts) {
        this.dirs += counts.dirs;
        this.files += counts.files;
        this.bytes += counts.bytes;
    }

    toString() {
        return `dirs: ${this.dirs}, files: ${this.files}, bytes: ${this.bytes}`;
    }
}

function summary(dirpath) {
    console.log('summary(%s)', dirpath);
    const cnt = new Counts();

    readdirSync(dirpath).forEach(filename => {
        const path = join(dirpath, filename);
        const meta = statSync(path);
        if (meta.isFile()) {
            cnt.updateFile( path );
        } else if (meta.isDirectory()) {
            cnt.dirs++;
            const result = summary(path);
            cnt.updateCounts(result);
            console.log('   %s: %s', dirpath, result);
        }
    });

    return cnt;
}

const dir = process.argv[2] || dirname(fileURLToPath(import.meta.url));

if (!statSync(dir).isDirectory) {
    console.error('not a directory: %s', dir);
    process.exit(1);
}

console.log('processing %s ...', normalize(dir));
const result = summary(dir);
console.log('TOTAL: %s', result);

